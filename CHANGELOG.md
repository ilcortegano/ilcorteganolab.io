# Change log

## Version 1.2.3 - 26 Dec 2024

- Added wedding page.
- Removed *Google Analytics* script.

## Version 1.2.2 - 23 Apr 2023

- Added filter for the Project section.

## Version 1.2.1 - 22 Apr 2023

- Updated the Experience main section.
- Updated the Experience main section design.

## Version 1.2.0 - 22 Apr 2023

- Added *Google Analytics* script.

## Version 1.1.0 - 6 Apr 2023

- Finished *ADS* blog entry.
- Added *ADS* bibliography.

## Version 1.0.9 - 6 Apr 2023

- Added *ADS - Map* blog.
- Updated push time charts.

## Version 1.0.8 - 1 Apr 2023

- Added *ADS - Set* blog.

## Version 1.0.7 - 31 Mar 2023

- Removed LinkedIn Profile.
- Changed experience section and some texts.

## Version 1.0.6 - 04 Mar 2023

- Added *ADS - Vector* blog.
- Changed memory space images on all *ADS*.

## Version 1.0.5 - 04 Mar 2023

- Added *ADS - List* blog.

## Version 1.0.4 - 21 Feb 2023

- Added *ADS - Heap* blog.
- Improved *ADS* blog design.

## Version 1.0.3 - 11 Feb 2023

- Added *ADS - Queue* blog.
- Improved *ADS* blog design.
- Fixed typos.
- Fixed broken Linked-In link.
- Updated CV.

## Version 1.0.2 - 29 Jan 2023

- Updated *ADS* blog, avaiable *ADS* articles are now listed in a table.

## Version 1.0.1 - 18 Jan 2023

- Added stacks blog entry.

## Version 1.0.0 - 15 Jan 2023

- Added data-structures blog page.
- Changelog created.
