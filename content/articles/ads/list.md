---
title: Abstract Data Structures; The List
date: 2023-03-04T00:00:00+00:00
draft: false
github_link: https://gitlab.com/ilcortegano/ads
author: Isauro López-Cortegano
description: ""
toc: 
---

# List

**Lists** are a type of container designed to store data in an unordered container. :mortar_board:

It can be imagined as a numbered collection of items. Depending on its design, we can distinguish between: **Lists** and **Vectors**. Each of these have a context in which they excel.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/list/list.png" alt="lists image"/>
</p>

## Design

Lists are intended to work efficiently in contexts where deletion and insertion of elements in arbitrary positions is frequent.
Whereas vectors are designed to work in contexts where the access and modification of information must be very efficient.

That is why each one has its own interface, as we will see below.

### Vector Container

For the vector design we will make use of an `Array`.
`Arrays` are a finite collection of elements of the same type. We can graphically represent an array as a sequence o boxes, each one can store a value, as shown in the image. These elements can be accesed using an index and it is very efficient since elements are stored together, one after the other, in a big chunk of memory.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/array.png" alt="array container"/>
</p>

In order to manage data efficiently, data storage must be performed in a structured way. For this, we will make use of two variables: `elements` and `capacity`.

- `elements`: indicates how many elements are present in the array, it can also be used as index where the next item will be inserted.
- `capacity`: represents the maximum number of elements that can be stored in the array.

Since the capacity of an array is finite, if we keep on inserting elements it is a matter of time until the array gets full.
When this happens, a bigger array is needed, the capacity can be doubled the elements copied to the new array.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/array-grow.png" alt="array resize growth"/>
</p>

Similarly, when the array is sufficiently empty, its capacity can be decreased to free resources that are not being used.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/array-shrink.png" alt="array resize shrink"/>
</p>

#### Vector Interface

The vector interface will define four basic operations:

- Insert data, from now on: *push_back(e: Element) -> void*.
- Erase data: *pop_back() -> void*.
- Read the element at a given index: *at(i: number) -> Element*.
- Count the number of elements: *size() -> number*.

### List Container

For the list design we will make use of a `Linked List of Nodes`.
`Nodes` are structures of information. For this design, the nodes defines the following variables:

- `element`: data.
- `previous`: A link to the previous `node` of the chain.
- `next`: A link to the next `node` of the chain.

For each new element to be stored in the `list`, a new `node` will be created.
`Nodes` will form a linked chain, to manage the nodes in an efficient way, we will make use of the following variables:

- `first`: A link to the first `node` of the linked chain.
- `last`: A link to the last `node` of the linked chain.
- `elements`: indicates how many elements are present in the linked chain.

The image below illustrates the behavior of a `linked list` as it grows.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/list/list_grow.png" alt="list growth"/>
</p>

The deletion of elements and consequently, the shrinking of the linked list, can be understood as the opposite of the illustrated one.
Contrary to arrays, linked lists of nodes are unbounded and do not need resize operations since the container grows and shrinks dinamically as requested. 

The internal elements of the list are not easily accessible, since the container has no efficient way of locating them through an index. To locate a node it is necessary to search it following the links of the chain.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/list/list_insert.png" alt="list insertion"/>
</p>

The cost of the search is linear (*O(n)*). Break the chain at a given point and link a new node between two existing nodes is very efficient (*O(1)*). Nevertheless, assuming a linear cost each time an insertion or deletion at an arbitrary index takes place is just as expensive as using the array container.

In the array, the search of the index is immediate (*O(1)*), but it would be necessary to move along the existing elements to not overwrite the elements already stored (*O(n)*).

At the end of the day, both containers have linear cost regarding insertions at arbitrary places, so what is the point on using linked lists of nodes?

#### Iterators

An `Iterator` is a structure that points to an element of a container. The iterator has the hability to navigate trough the container and execute operations. For this design, the iterator makes use of the variable `current`.

- `current`: A link to a `node` of the chain.

The iterator allows to define functions to efficiently insert, delete and edit nodes.

#### List Interface

The list interface will define eleven basic operations:

- Insert data at the end of the chain, from now on: *push_back(e: Element) -> void*.
- Insert data at the begin of the chain: *push_front(e: Element) -> void*.
- Erase the last node of the chain: *pop_back() -> void*.
- Erase the first node of the chain: *pop_front() -> void*.
- Read the head of the list: *front() -> Element*.
- Read the back of the list: *back() -> Element*.
- Count the number of elements: *size() -> number*.
- Search an element: *find(e: Element) -> Iterator*.
- Search an element by index: *at(i: number) -> Iterator*.
- Insert an element where the iterator is: *insert(e: Element, i: Iterator) -> void*.
- Erase an element pointed by the iterator: *erase(i: Iterator) -> Iterator*.

### Design Tradeoffs: List vs Vector

Most relevant tradeoffs are listed below:

<style>
	.prosTable tr {
		background: green;
	}
	.consTable tr {
		background: red;
	}
</style>

<div class="prosTable">

| List | Vector |
| ---- | ------ |
| Efficient chain management.       | Quick and efficient access to the data stored within the container. |
| Efficient regarding memory space. | Efficient regarding memory allocation.                              |

</div>

<div class="consTable">

| List | Vector |
| ---- | ------ |
| Operations allocate and de-allocate memory. | Chain managment is not efficient. |
| Elements are scattered in memory.           | Uses a chunk of memory. |

</div>

##### Other Stack Designs

The standard design and specification of the [list](https://en.cppreference.com/w/cpp/container/list) and [vector](https://en.cppreference.com/w/cpp/container/vector) is very well documented at [cppreference](https://en.cppreference.com).

Do not doubt on checking up the docs, they're amazing!

## Implementation

The following speudo-code outlines the realization of the `list` interface with the proposed design.

```js
// Auxiliar function.
const insertNode = function(e: Element, nA: Node, nC: Node) {
	Node nB = new Node(nA, e, nC); // Allocate the new node.

	if (nA != null) nA->next = nB;     // If 'nA' exists, link its next node to the new node.
	if (nC != null) nC->previous = nB; // If 'nC' exists, link its previous node to the new node.

	elements = elements + 1; // Count the new element.
	return nB;               // Return a pointer to the new node.
}

// Auxiliar function.
const deleteNode = function(n: Node) {
	if (n->previous != null) n->previous->next = n->next;     // Links the 'next' property of the previous.
	if (n->next != null)     n->next->previous = n->previous; // Links the 'previous' property of the next.

	elements = elements - 1;  // Decrease the count of elements.
	delete n;                 // De-allocate the memory held by the node.
}

function push_front(e: Element) {
	first = insertNode(e, null, first); // Create a new node placed at the begin.

	if (last == null) last = first;     // Update the 'last' pointer if needed.
}

function pop_front() {
	if (first == null) // If the chain is empty an error occurs.
		throw error;

	Node *newFirst =  first->next; // Save a reference to the new 'first' node.
	deleteNode(first);             // Delete the first node.

	first = newFirst;              // Update the pointers.
	if (newFirst == null) last = null;
}

function push_back(e: Element) {
	last = insertNode(e, last, null); // Create a new node placed at the end.

	if (first = null) first = last;   // Update the 'first' pointer if needed.
}

function pop_back() {
	if (last == null) // If the chain is empty an error occurs.
		throw error;

	Node *newLast = last->previous; // Save a reference to the new 'last' node.
	deleteNode(last);               // Delete the last node.

	last = newLast;                 // Update the pointers.
	if (newLast == null) first = null;
}

function front() {
	if (first == null) // If the chain is empty an error occurs.
		throw error;

	return first->element; // Read the element of the 'first' node.
}

function back() {
	if (last == null) // If the chain is empty an error occurs.
		throw error;

	return last->element; // Read the element of the 'last' node.
}

function size() {
	return elements; // Return the number of elements.
}

function at(index: number) {
	if (pos >= elements) // If the index is out of range an error occurs.
		throw error;

	Node *curr = first;  // Iterate until the pointer is found.
	for (let i = 0; i < index; ++i)
		curr = curr->next;

	return new Iterator(curr); // Returns an iterator pointing at the node.
}

function find(e: Element) {
	if (first == null)  // If the chain is empty an error occurs.
		throw error;

	Node *curr = first; // Iterate until the element is found.
	while (curr != null && curr->element != e)
		curr = curr->next;

	return new Iterator(curr); // Returns an iterator.
}

function erase(it: Iterator) {
	if (first == null) // If the chain is empty an error occurs.
		throw error;

	// Delete the node and return an appropriate iterator.
	if (it.current == first) {
		pop_front();
		return new Iterator(first);
	}

	if (it.current == last) {
		pop_back();
		return new Iterator(null); // Returns an iterator.
	}

	Node *curr = it.current->next;
	deleteNode(it.current);
	return Iterator(curr); // Returns an iterator.
}

function insert(e: Element, it: Iterator) {

	// Insert the element at the place pointer by the iterator.
	if (it.current == first)     { push_front(elem); }
	else if (it.current == null) { push_back(elem);  }
	else { insertNode(elem, it.current->previous, it.current); }
}
```

The following speudo-code outlines the realization of the `vector` interface with the proposed design.

```js
function push_back(e: Element) {
	if (elements == capacity) // If the container is full we double its size.
		grow();

	container[elements] = e;  // Store the element at the position indexed by the number of elements
	elements = elements + 1;  // Increase the count of elements stored.
}

function pop_back() {
	if (elements == 0) // If the container is empty an error occurs.
		throw error;

	elements = elements - 1;         // Decrease the count of elements stored.
	if (elements < ( capacity / 4 )) // If the container is mostly empty we shrink it.
		shrink();
}

function at(i: number) {
	if (i >= elements) // If the index is out of range an error occurs.
		throw error;

	return container[i]; // Read the position indexed by 'i'.
}

function size() {
	return elements; // Return the number of elements.
}
```

## Performance

### List: Operation Costs

<div align="center">

   | Name         | *O*    |  Purpose                                          |
   | ------------ | ------ | ------------------------------------------------- |
   | *push_back*  | *O*(1) | Inserts an element at the back of the chain.      |
   | *push_front* | *O*(1) | Inserts an element on the head of the chain.      |
   | *pop_back*   | *O*(1) | Delete the back of the chain.                     |
   | *pop_front*  | *O*(1) | Deletes the head of the chain.                    |
   | *front*      | *O*(1) | Reads the element at the head of the linked list. |
   | *back*       | *O*(1) | Reads the element at the tail of the linked list. |
   | *size*       | *O*(1) | Gets the number of elements in the container.     |
   | *at*         | *O*(n) | Gives an iterator pointing at an index.           |
   | *find*       | *O*(n) | Gives an iterator pointing at an element.         |
   | *erase*      | *O*(1) | Given an iterator, deletes the node.              |
   | *insert*     | *O*(1) | Given an iterator, inserts a node.                |

</div>

### List: Benchmarks

To test the proposed design, the total time consumed by push and pop operations was timed by performing 300.000 insertions and deletions. These results are contrasted with those obtained using the standard implementation.
Note that the repository code is not exception-safe and should not be used in production environments.

The overall performance is summarized in this table.

<div align="center">

   |              | ilc::ads:list | std::list    | Description                               |
   | ------------ | ------------- | ------------ | ----------------------------------------- |
   | *push_back*  | 132 *ms*      | 134 *ms*     | Time taken to perform 300.000 insertions. |
   | *pop_back*   | 11.10 *s*     | 11.10 *s*    | Time taken to perform 300.000 deletions.  |

</div>

The chart below ilustrates the time taken by the operations, the time cost remains constant no matter the number of elements are inserted or deleted.

To make the chart clearer, the peaks have been cut off.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/list/push_time.png" alt="insert time"/>
</p>
<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/list/pop_time.png" alt="erase time"/>
</p>

The space allocated in memory is shown in the charts below; It can be seen how memory grows at a constant rate.

<div class="row">
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/list/push_space.png" alt="insert space"/>
  </div>
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/list/pop_space.png" alt="erase space"/>
  </div>
</div>

### Vector: Operation Costs

<div align="center">

   | Name         | *O*    | *Θ*    |  Purpose                                      |
   | ------------ | ------ | ------ | --------------------------------------------- |
   | *push_back*  | *O*(n) | *Θ*(1) | Inserts an element at the back of the chain.  |
   | *pop_back*   | *O*(n) | *Θ*(1) | Delete the back of the chain.                 |
   | *at*         | *O*(1) | *Θ*(1) | Gives an iterator pointing at an index.       |
   | *size*       | *O*(1) | *Θ*(1) | Gets the number of elements in the container. |

</div>

### Vector: Benchmarks

To test the proposed design, the total time consumed by push and pop operations was timed by performing 300.000 insertions and deletions. These results are contrasted with those obtained using the standard implementation.
Note that the repository code is not exception-safe and should not be used in production environments.

The overall performance is summarized in this table.

<div align="center">

   |              | ilc::ads:vector                                       | std::vector    | Description                               |
   | ------------ | ----------------------------------------------------- | -------------- | ----------------------------------------- |
   | *push_back*  | 660 *us*                                              | 1637 *us*      | Time taken to perform 300.000 insertions. |
   | *pop_back*   | 375 *us* (54 *us* if container does not shrink)       | 54 *us*        | Time taken to perform 300.000 deletions.  |

</div>

The chart below ilustrates the time taken by the operations, the time cost remains constant no matter the number of elements are inserted or deleted. Plus, spikes reveal when resize operations where made due to their high cost.

To make the chart clearer, the peaks have been cut off.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/vector/push_time.png" alt="insert time"/>
</p>
<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/vector/pop_time_no_opt.png" alt="erase time"/>
</p>

If the container is programed not to shrink when it is mostly empty, the nano-seconds taken per *pop_back* operation greatly decreases (the average time per operation is up to seven times faster!). The chart below ilustrates the *pop_back* time performance with a container that never shrinks.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/vector/pop_time.png" alt="erase time"/>
</p>

The space allocated in memory is shown in the charts below; It can be seen how memory grows and shrinks by chunks.

<div class="row">
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/vector/push_space.png" alt="insert space"/>
  </div>
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/vector/pop_space.png" alt="erase space"/>
  </div>
</div>
