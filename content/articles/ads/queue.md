---
title: Abstract Data Structures; The Queue
date: 2023-02-11T00:00:00+00:00
draft: false
github_link: https://gitlab.com/ilcortegano/ads
author: Isauro López-Cortegano
description: ""
toc: 
---

# Queues

**Queues** are a type of container designed to operate in a **FIFO** (*First in, first out*) context. :mortar_board:

It can be imagined as a line of people, in which the first to arrive is served first and the last ones wait their turn.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/queue/queue.png" alt="queues image"/>
</p>

## Design

The interface will define five basic operations:

- Insert data, from now on: *push(e: Element) -> void*.
- Erase data: *pop() -> void*.
- Read the first item of the queue: *front() -> Element*.
- Read the last item on the queue: *back() -> Element*.
- Count the number of elements: *size() -> number*.

### Container

For the queue design we will make use of an `Array`.
`Arrays` are a finite collection of elements of the same type. We can graphically represent an array as a sequence o boxes, each one can store a value, as shown in the image. These elements can be accesed using an index and it is very efficient since elements are stored together, one after the other, in a big chunk of memory.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/array.png" alt="array container"/>
</p>

In order to manage data efficiently, data storage must be performed in a structured way. For this, we will make use of two variables: `elements` and `capacity`.

- `elements`: indicates how many elements are present in the array.
- `capacity`: represents the maximum number of elements that can be stored in the array.

Since the capacity of an array is finite, if we keep on inserting elements it is a matter of time until the array gets full.
When this happens, a bigger array is needed, the capacity can be doubled the elements copied to the new array.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/array-grow.png" alt="array resize growth"/>
</p>

Similarly, when the array is sufficiently empty, its capacity can be decreased to free resources that are not being used.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/array-shrink.png" alt="array resize shrink"/>
</p>

To mirror the behaviour of a queue, it is needed to use the variables `begin` and `end`, which denotes the range of indexes where the elements are located in the array.

- `begin`: the index of the first element of the queue.
- `end`: the index of the last element of the queue.

New elements will be stored at the `end` index, while deletion will occur at the indexes pointed by the `begin` variable. As long as the total number of elements is lower or equal than the `capacity`, the array will function as a `circular array`. The image below ilustrates the behaviour of a `circular array`.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/circular-array.png" alt="circular array"/>
</p>

### Design Tradeoffs

Most relevant tradeoffs are listed below:

<style>
	.prosTable tr {
		background: green;
	}
	.consTable tr {
		background: red;
	}
</style>

<div class="prosTable">

| Pros |
| ---- |
| Save time on insertions and deletions by having a chunk of memory allocated until the container needs to be resized. |
| Quick and efficient access to the data stored within the array.                                                      |

</div>
<div class="consTable">

| Cons |
| ---- |
| By choosing an array as container we renounce to occupie the minimum and necessary amount of memory required. |
| Runs ouf of memory quicker than other designs.                                                    |

</div>

### Other Queue Designs

The standard design and specification of the [queue](https://en.cppreference.com/w/cpp/container/queue) is very well documented at [cppreference](https://en.cppreference.com).

Do not doubt on checking up the info, it is amazing!

## Implementation

The following speudo-code outlines the realization of the interface with the proposed design.

```js
function push(e: Element) {
	if (elements == capacity) // If the container is full we double its size.
		grow();

	if (elements != 0) {            // If the container is not empty
		end = (end + 1) % capacity; // .. update the 'end' pointer.
	}
	container[end] = e;      // Store the element at the position indexed by the number of elements
	elements = elements + 1; // Increase the count of elements stored.
}

function pop() {
	if (elements == 0) // If the container is empty an error occurs.
		throw error;

	begin = (begin + 1) % capacity;    // Update the 'begin' pointer.

	elements = elements - 1;           // Decrease the count of elements stored.
	if (elements < ( capacity / 4 )) { // If the container is mostly empty ..
		shrink();                      // .. we shrink it.
	}
}

function front() {
	if (elements == 0) // If the container is empty an error occurs.
		throw error;

	return container[begin]; // Read the position indexed by 'begin'.
}

function back() {
	if (elements == 0) // If the container is empty an error occurs.
		throw error;

	return container[end]; // Read the position indexed by 'end'.
}

function size() {
	return elements; // Return the number of elements.
}
```

## Performance

### Operation Costs

Because of the `array` container, our worst case scenario occurs when, modifying the data, the container needs to be resized.
A copy of N elements must be performed in order to persist the data within the container. However, the amortized cost when pushing and poping data is constant *Θ*(1).

<div align="center">

   | Name    | *O*    | *Θ*    | Purpose                                      |
   | ------- | ------ | ------ | -------------------------------------------- |
   | *push*  | *O*(n) | *Θ*(1) | Inserts an element at the back of the queue. |
   | *pop*   | *O*(n) | *Θ*(1) | Deletes the front element of the queue.      |
   | *front* | *O*(1) | *Θ*(1) | Reads the front of the queue.                |
   | *back*  | *O*(1) | *Θ*(1) | Reads the back of the queue.                 |
   | *size*  | *O*(1) | *Θ*(1) | Gets the count of elements in the container. |

</div>

### Benchmarks

To test the proposed design, the total time consumed by push and pop operations was timed by performing 300.000 insertions and deletions. These results are contrasted with those obtained using the standard implementation.
Note that the repository code is not exception-safe and should not be used in production environments.

The overall performance is summarized in this table.

<div align="center">

   |        | ilc::ads:queue | std::queue | Description                               |
   | ------ | -------------- | ---------- | ----------------------------------------- |
   | *push* | 1318 *μs*      | 2130 *μs*  | Time taken to perform 300.000 insertions. |
   | *pop*  | 1097 *μs*      | 54.72 *ms* | Time taken to perform 300.000 deletions.  |

</div>

The chart below ilustrates clearly the behaviour designed, we can see how cost remains constant no matter the number of elements are inserted or deleted. Plus, spikes reveal when resize operations where made due to their high cost.

To make the chart clearer, the peaks have been cut off.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/queue/push_time.png" alt="push time"/>
</p>
<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/queue/pop_time.png" alt="pop time"/>
</p>

The space allocated in memory is shown in the charts below; It can be seen how memory grows and shrinks by chunks.

<div class="row">
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/queue/push_space.png" alt="push space"/>
  </div>
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/queue/pop_space.png" alt="pop space"/>
  </div>
</div>
