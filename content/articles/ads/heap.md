---
title: Abstract Data Structures; The Heap
date: 2023-02-20T00:00:00+00:00
draft: false
github_link: https://gitlab.com/ilcortegano/ads
author: Isauro López-Cortegano
description: ""
toc: 
---

# Heaps

**Heaps** are a type of container designed to operate in a **PRIO** (*In order of priority*) context. :mortar_board:

It can be imagined as a group of people in which the highest priority individual is attended first.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/heap/heap.png" alt="heaps image"/>
</p>

## Design

The heap interface defines four basic operations:

- Insert data, from now on: *push(e: Element) -> void*.
- Erase the most imporant element: *pop() -> void*.
- Get the most important element: *next() -> Element*.
- Count the number of elements: *size() -> number*.

### Container

For the heap design we will make use of an `Array`.
`Arrays` are a finite collection of elements of the same type. We can graphically represent an array as a sequence o boxes, each one can store a value, as shown in the image. These elements can be accesed using an index and it is very efficient since elements are stored together, one after the other, in a big chunk of memory.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/array.png" alt="array container"/>
</p>

In order to manage data efficiently, data storage must be performed in a structured way. For this, we will make use of two variables: `elements` and `capacity`.

- `elements`: indicates how many elements are present in the array.
- `capacity`: represents the maximum number of elements that can be stored in the array.

Since the capacity of an array is finite, if we keep on inserting elements it is a matter of time until the array gets full.
When this happens, a bigger array is needed, the capacity can be doubled the elements copied to the new array.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/array-grow.png" alt="array resize growth"/>
</p>

Similarly, when the array is sufficiently empty, its capacity can be decreased to free resources that are not being used.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/array-shrink.png" alt="array resize shrink"/>
</p>

In order to use an array as a heap, elements need to be stored in a sophisticated way. We cannot simply store the element in an ordered, nor an unordered way and expect to find the most prioritary element in an efficient way.

<div align="center">

   |                       | *push* | *next* | pop    |
   | --------------------- | ------ | ------ | ------ |
   | *unordered container* | *Θ*(1) | *Θ*(n) | *Θ*(n) |
   | *ordered container*   | *Θ*(n) | *Θ*(1) | *Θ*(1) |

</div>

A linear cost may seem fair, but it can be done better. The most prioritary element will be placed as the first element of the container, the index 0. The heap must respect the invariant:

```
heap(h) = empty(h) ∨ (h = compose(i; x; d)) ∧ heap(i) ∧ heap(d) ∧
		     (¬empty(l)) => x ≤ root(l)) ∧
		     (¬empty(r)) => x ≤ root(r))
```

The previous statement indicates that a container `h` is a heap if and only if: `h` is empty or the root element of `h`, `x`, is smaller than the root of its left `l` and right `r` children, in addition, these children must also be heaps. If we want a heap where maximum takes priority, replace ≤ by a ≥ in the above definition. In this design, we will represent graphically the heap using a binary tree.

The following image ilustrates the growth of a heap. New elements are inserted at the end of the container and are lifted until the invariant is satisfied.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/heap/heap_insert.png" alt="heap insert"/>
</p>

On the other hand, the deletion process of an element starts by placing as the root element the last element the container. Then, the value sinks to the bottom of the heap until the invariant turns true.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/heap/heap_delete.png" alt="heap delete"/>
</p>

### Design Tradeoffs

Most relevant tradeoffs are listed below:

<style>
	.prosTable tr {
		background: green;
	}
	.consTable tr {
		background: red;
	}
</style>

<div class="prosTable">

| Pros |
| ---- |
| Save time on insertions and deletions by having a chunk of memory allocated until the container needs to be resized. |
| Quick and efficient access to the data stored within the array.                                                      |
| Balanced cost between `push` and `pop` operations.                                                                   |

</div>
<div class="consTable">

| Cons |
| ---- |
| By choosing an array as container we renounce to occupie the minimum and necessary amount of memory required. |
| Runs ouf of memory quicker than other designs.                                                    |

</div>

##### Other Heap Designs

The standard design and specification of a [heap](https://en.cppreference.com/w/cpp/container/priority_queue) is very well documented at [cppreference](https://en.cppreference.com).

Do not doubt on checking up the docs, they're amazing!

## Implementation

The following speudo-code outlines the realization of the interface with the proposed design.

```js
// Functions to be used for heaps where the lower element takes higher priority.
const hasToLift = function(e1: Element, e2: Element) const { return e1 < e2; }
const hasToSink = function(e1: Element, e2: Element) const { return e1 > e2; }

// Functions to be used for heaps where the higher element takes higher priority.
const hasToLift = function(e1: Element, e2: Element) const { return e1 > e2; }
const hasToSink = function(e1: Element, e2: Element) const { return e1 < e2; }

function push(e: Element) {
	if (elements == capacity) // If the container is full we double its size.
		grow();

	let place: int = elements;                   // The new element should be inserted here.
	let middle: int = (elements - 1) / 2;        // Index of the sub-heap root where the new element is going to be inserted.

	while (place > 0 && hasToLift(e, container[middle])) { // If 'e' has priority on the current sub-heap ..

		container[place] = container[middle];    // we sink the root element of the sub-heap..
		place = middle;                          // ..we update the placement position for the new element and..
		middle = (middle - 1) / 2;               // ..we update the new sub-heap root index.
	}

	container[place] = e;     // Store the element at the position indexed by 'place'.
	elements = elements + 1;  // Increase the count of elements stored.
}

function pop() {
	if (elements == 0) // If the container is empty an error occurs.
		throw error;

	let hole: int = 0;       // The sub-heap root index where to lift a new value.
	let son: int = 2;        // Index of the sub-heap root's right child.
	let aHeap: bool = false; // Is the current container a heap?

	elements = elements - 1;               // Decrease the count of elements stored.
	container[hole] = container[elements]; // Place as the most important element, the element indexed by 'elements'.

	while (son <= elements && !aHeap) {    // As long as the array container does not satisfy the heap invariant ..

		if (son != elements && hasToLift(container[son], container[son - 1])) { // ..we first check which child need to lift its root..
			son = son + 1;                                                      // .. in case it is the right child, we update the index.
		}

		if (hasToSink(container[hole], container[son - 1])) { // We then check the heap's invariant.
			let temp: Element = container[hole];              // Store the value of the element to sink..

			container[hole] = container[son - 1];   // ..lift the sub-heap root element as the new root of the heap and..
			hole = son - 1;                         // ..update the 'hole' index.
			container[hole] = temp;                 // Sink the value stored and..
			son = son * 2;                          // ..update the 'son' index.
		}
		else {                                      // If the container is a heap..
			aHeap = true;                           // ..we're done lifting values.
		}
	}

	if (elements < ( capacity / 4 )) // If the container is mostly empty we shrink it.
		shrink();
}

function next() {
	if (elements == 0) // If the container is empty an error occurs.
		throw error;

	return container[0];    // Read the most important element.
}

function size() {
	return elements; // Return the number of elements.
}
```

## Performance

### Operation Costs

Because of the `array` container, our worst case scenario occurs when, modifying the data, the container needs to be resized.
A copy of N elements must be performed in order to persist the data within the container. However, the amortized cost when pushing and poping data is logarithmic *Θ*(log(n)).

<div align="center">

   | Name   | *O*    | *Θ*    | Purpose                                      |
   | ------ | ------ | ------ | -------------------------------------------- |
   | *push* | *O*(n) | *Θ*(log(n)) | Inserts an element on the container.    |
   | *pop*  | *O*(n) | *Θ*(log(n)) | Deletes the most important element.     |
   | *next* | *O*(1) | *Θ*(1) | Reads the most important element.            |
   | *size* | *O*(1) | *Θ*(1) | Gets the count of elements in the container. |

</div>

### Benchmarks

To test the proposed design, the total time consumed by push and pop operations was timed by performing 300.000 insertions and deletions. These results are contrasted with those obtained using the standard implementation.
Note that the repository code is not exception-safe and should not be used in production environments.

The overall performance is summarized in this table.

<div align="center">

   |        | ilc::ads:stack | std::stack  | Description                               |
   | ------ | -------------- | ----------- | ----------------------------------------- |
   | *push* | 1319 *μs*      | 1402 *μs*   | Time taken to perform 300.000 insertions. |
   | *pop*  | 1282 *μs*      | 4105 *μs*   | Time taken to perform 300.000 deletions.  |

</div>

The chart below ilustrates clearly the behaviour designed, we can see how the cost seems to remain constant, although its logarithmic, no matter the number of elements are inserted or deleted. Plus, spikes reveal when resize operations where made due to their high cost.

To make the chart clearer, the peaks have been cut off.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/heap/push_time.png" alt="push time"/>
</p>
<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/heap/pop_time.png" alt="pop time"/>
</p>

The space allocated in memory is shown in the charts below; It can be seen how memory grows and shrinks by chunks.

<div class="row">
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/heap/push_space.png" alt="push space"/>
  </div>
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/heap/pop_space.png" alt="pop space"/>
  </div>
</div>
