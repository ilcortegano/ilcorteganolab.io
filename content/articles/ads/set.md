---
title: Abstract Data Structures; The Set
date: 2023-04-01T00:00:00+00:00
draft: false
github_link: https://gitlab.com/ilcortegano/ads
author: Isauro López-Cortegano
description: ""
toc: 
---

# Set

**Sets** are a type of container that stores a collection of unrepeated data. :mortar_board:

They behave just like mathematical sets.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/set/set.png" alt="sets image"/>
</p>

## Design

The set interface defines four basic operations:

- Insert an element onto the set: *insert(e: Element) -> void*.
- Erase an element: *erase(e: Element) -> void*.
- Check if an element is present in the set: *contains(e: Element) -> boolean*.
- Count the number of elements: *size() -> number*.

### Container

For the set design a `Binary Tree`, more specifically a `Red-Black Tree`, can be used as container.

`Binary Trees` are tree data structures in which each parent `node` can have at most two children.
`Nodes` are structures of information. For this design, nodes defines the following variables:

- `element`: Data.
- `left`: A link to the left `node`.
- `right`: A link to the right `node`.

A binary tree must respect the invariant:

```
tree(n) = empty(n) ∨ (elem(n.left) < elem(n) < elem(n.right) ∧ tree(n.left) ∧ tree(n.right))
```

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/tree.png" alt="tree container"/>
</p>

Unlike linear data structures (Array, Linked List, Queues, Stacks, etc) which have only one logical way to traverse them, trees can be traversed in different ways. The following are the generally used methods for traversing trees:

<div align="center">

|          |                           |
| -------- | ------------------------- |
| InOrder  | 6, 11, 15, 23, 24, 38, 54 |
| PreOrder | 23, 11, 6, 15, 38. 24, 54 |
| PosOrder | 6, 15, 11, 24, 54, 38, 23 |

</div>

For each new element to be stored in the `binary tree`, a new `node` will be created.
To manage the nodes in an efficient way, we will make use of the following variables:

- `root`: A link to the last `root` node of the `tree`.
- `elements`: indicates how many elements are present in the `tree`.

The image below illustrates the behavior of a `binary tree` as it grows.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/tree_grow.png" alt="tree growth"/>
</p>

This data structure is efficient because it offers a logarithmic cost when locating, inserting and deleting elements.
If no further specification is provided, and we make use of a pure binary tree, the following may occur for successive ordered data insertions.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/tree_bad.png" alt="tree unbalanced"/>
</p>

In this case, the binary tree, although respecting the invariant, loses its effectiveness as a logarithmic structure, since it is used as a linked list of nodes and the cost of operations becomes linear.

To control these situations, `AVL trees` and `Red-Black trees` incorporate node balancing mechanisms, which correct malformations in the binary trees as elements are inserted and deleted.
This guarantees a logarithmic cost for the operations.
Some of the situations in which a binary tree gets unbalanced are illustrated below. For this, `Nodes` are tagged with a red or black `color` property to help identify these situations.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/tree_balance.png" alt="tree balanced"/>
</p>

### Design Tradeoffs

Most relevant tradeoffs are listed below:

<style>
	.prosTable tr {
		background: green;
	}
	.consTable tr {
		background: red;
	}
</style>

<div class="prosTable">

| Pros |
| ---- |
| Cost remains log(n) no matter the size of the tree. |
| Efficient regarding memory space. |

</div>
<div class="consTable">

| Cons |
| ---- |
| Hard to implement. |
| Operations allocate and de-allocate memory. |
| Elements are scattered in memory.           |

</div>

##### Other Set Designs

The standard design and specification of the [set](https://en.cppreference.com/w/cpp/container/set) is very well documented at [cppreference](https://en.cppreference.com).

Do not doubt on checking up the docs, they're amazing!

## Implementation

The following speudo-code outlines the realization of the interface with the proposed design.

```js
// Auxiliar function.
const insertElement = function(n: Node, e: Element) {
	if (n == null) {             // If tree 'n' is empty..
		elements = elements + 1; // Count the new element.
		return new Node(e);      // Allocate the new node.
	} 

	if (isRed(n->left) && isRed(n->right)) // Break 4-nodes
		colorFlip(n);

	if (n->element == e) {       // If the element already exists..
								 // we do nothing,
	} else if (e < n->element) { // otherwise, 'e' is inserted in a sub-tree.
		n->left = insertElement(n->left, e);
	} else {
		n->right = insertElement(n->right, e);
	}

	// Rotates the tree if corrections are needed.
	if (isRed(n->right))
		n = rotateLeft(n);

	// Rotates the tree if corrections are needed.
	if (isRed(node->left) && isRed(n->left->left))
		n = rotateRight(n);

	return n; // Return the root of the current sub-tree.
}

// Auxiliar function.
const deleteElement = function(n: Node, e: Element) {

	if (e < n->element) { // if the element to erase is on the left children..
		// Rotates the tree if corrections are needed.
		if (!isRed(n->left) && !isRed(n->left->left))
			n = moveRedLeft(n);

		// Delete the element in the left sub-tree.
		n->left = deleteElement(n->left, e);
	} else {
		// Rotates the tree if corrections are needed.
		if (isRed(n->_left))
			n = rotateRight(n);

		if (e == n->element && n->right == null) { // if 'e' found and has no right childs..
			elements = elements - 1; // Discount the element.
			delete n;                // De-allocate the memory held by the node.
			return null;
		}

		// Rotates the tree if corrections are needed.
		if (!isRed(n->right) && !isRed(n->right->left))
			n = moveRedRight(n);

		if (e == n->element) {              // if 'e' is found..
			n->element = min(n->right);     // overwrite 'e' with the minimum of sub-tree..
			n->right = deleteMin(n->right); // then, erase the minimum of the subtree.
		} else {
			n->right = deleteElement(n->right, e); // otherwise, look over the right node.
		}
	}
	return fixUp(n); // fix red-back invariant

}

function insert(e: Element) {

	root = insertElement(root, e); // Insert element e onto the tree.
}

function erase(e: Element) {

	root = deleteElement(root, e); // Delete element e from the tree.
}

function contains(e: Element) {
	Node curr = root;                                      // From the root node..

	while (curr != null && e != curr->element) {           // navigate to the left or right children..
		curr = e < curr->element? curr->left: curr->right; // looking for the element 'e'.
	}
	return curr != null;                                   // return true if found, false otherwise.
}

function size() {
	return elements; // Return the number of elements.
}
```

## Performance

### Operation Costs

<div align="center">

   | Name        | *O*         | Purpose                                      |
   | ----------- | ----------- | -------------------------------------------- |
   | *insert*    | *O*(log(n)) | Inserts an element onto the set.             |
   | *erase*     | *O*(log(n)) | Deletes an element from the set.             |
   | *contains*  | *O*(log(n)) | Checks if an element exists in the set.      |
   | *size*      | *O*(1)      | Gets the count of elements in the container. |

</div>

### Benchmarks

To test the proposed design, the total time consumed by insert and erase operations was timed by performing 300.000 insertions and deletions. These results are contrasted with those obtained using the standard implementation.
Note that the repository code is not exception-safe and should not be used in production environments.

The overall performance is summarized in this table.

<div align="center">

   |          | ilc::ads:set | std::set  | Description                               |
   | -------- | ------------ | --------- | ----------------------------------------- |
   | *insert* | 167 *ms*     | 165 *ms*  | Time taken to perform 300.000 insertions. |
   | *erase*  | 20.85 *s*    | 20.71 *s* | Time taken to perform 300.000 deletions.  |

</div>

The following insertion graph clearly illustrates the designed behavior.
It can be seen how the cost grows logarithmically as the number of elements grows.
In the case of the deletion graph, deleting nodes scattered in memory is time consuming, causing the logarithmic cost of the operation to be unappreciated. The deletion chart should look like a mirror image of the insertion one.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/set/insert_time.png" alt="insert time"/>
</p>
<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/set/erase_time.png" alt="erase time"/>
</p>

The space allocated in memory is shown in the charts below; It can be seen how memory grows at a constant rate as new nodes are being created or destroyed.

<div class="row">
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/set/insert_space.png" alt="insert space"/>
  </div>
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/set/erase_space.png" alt="erase space"/>
  </div>
</div>
