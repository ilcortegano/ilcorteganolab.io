---
title: Abstract Data Structures; The Stack
date: 2023-01-16T00:00:00+00:00
draft: false
github_link: https://gitlab.com/ilcortegano/ads
author: Isauro López-Cortegano
description: ""
toc: 
---

# Stacks

**Stacks** are a type of container designed to operate in a **LIFO** (*Last in, first out*) context. :mortar_board:

It can be imagined as a tower of plates, where it is only allowed to place and remove dishes from the top of the pile.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/stack/stacks.png" alt="stacks image"/>
</p>

## Design

The stack interface defines four basic operations:

- Insert data, from now on: *push(e: Element) -> void*.
- Erase data: *pop() -> void*.
- Read the top of the stack: *top() -> Element*.
- Count the number of elements: *size() -> number*.

### Container

For the stack design we will make use of an `Array`.
`Arrays` are a finite collection of elements of the same type. We can graphically represent an array as a sequence o boxes, each one can store a value, as shown in the image. These elements can be accesed using an index and it is very efficient since elements are stored together, one after the other, in a big chunk of memory.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/array.png" alt="array container"/>
</p>

In order to manage data efficiently, data storage must be performed in a structured way. For this, we will make use of two variables: `elements` and `capacity`.

- `elements`: indicates how many elements are present in the array, it can also be used as index where the next item will be inserted.
- `capacity`: represents the maximum number of elements that can be stored in the array.

Since the capacity of an array is finite, if we keep on inserting elements it is a matter of time until the array gets full.
When this happens, a bigger array is needed, the capacity can be doubled the elements copied to the new array.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/array-grow.png" alt="array resize growth"/>
</p>

Similarly, when the array is sufficiently empty, its capacity can be decreased to free resources that are not being used.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/array-shrink.png" alt="array resize shrink"/>
</p>

### Design Tradeoffs

Most relevant tradeoffs are listed below:

<style>
	.prosTable tr {
		background: green;
	}
	.consTable tr {
		background: red;
	}
</style>

<div class="prosTable">

| Pros |
| ---- |
| Save time on insertions and deletions by having a chunk of memory allocated until the container needs to be resized. |
| Quick and efficient access to the data stored within the array.                                                      |

</div>
<div class="consTable">

| Cons |
| ---- |
| By choosing an array as container we renounce to occupie the minimum and necessary amount of memory required. |
| Runs ouf of memory quicker than other designs.                                                    |

</div>

##### Other Stack Designs

The standard design and specification of the [stack](https://en.cppreference.com/w/cpp/container/stack) is very well documented at [cppreference](https://en.cppreference.com).

Do not doubt on checking up the docs, they're amazing!

## Implementation

The following speudo-code outlines the realization of the interface with the proposed design.

```js
function push(e: Element) {
	if (elements == capacity) // If the container is full we double its size.
		grow();

	container[elements] = e;  // Store the element at the position indexed by the number of elements
	elements = elements + 1;  // Increase the count of elements stored.
}

function pop() {
	if (elements == 0) // If the container is empty an error occurs
		throw error;

	elements = elements - 1;         // Decrease the count of elements stored.
	if (elements < ( capacity / 4 )) // If the container is mostly empty we shrink it.
		shrink();
}

function top() {
	if (elements == 0) // If the container is empty an error occurs.
		throw error;

	const top = elements - 1; // The top is indexed by the number of elements - 1.
	return container[top];    // Read the position indexed by 'top'.
}

function size() {
	return elements; // Return the number of elements.
}
```

## Performance

### Operation Costs

Because of the `array` container, our worst case scenario occurs when, modifying the data, the container needs to be resized.
A copy of N elements must be performed in order to persist the data within the container. However, the amortized cost when pushing and poping data is constant *Θ*(1).

<div align="center">

   | Name   | *O*    | *Θ*    | Purpose                                      |
   | ------ | ------ | ------ | -------------------------------------------- |
   | *push* | *O*(n) | *Θ*(1) | Inserts an element on top of the stack.      |
   | *pop*  | *O*(n) | *Θ*(1) | Deletes the element at the top.              |
   | *top*  | *O*(1) | *Θ*(1) | Reads the top of the stack.                  |
   | *size* | *O*(1) | *Θ*(1) | Gets the count of elements in the container. |

</div>

### Benchmarks

To test the proposed design, the total time consumed by push and pop operations was timed by performing 300.000 insertions and deletions. These results are contrasted with those obtained using the standard implementation.
Note that the repository code is not exception-safe and should not be used in production environments.

The overall performance is summarized in this table.

<div align="center">

   |        | ilc::ads:stack | std::stack  | Description                               |
   | ------ | -------------- | ----------- | ----------------------------------------- |
   | *push* | 1253 *μs*      | 2213 *μs*   | Time taken to perform 300.000 insertions. |
   | *pop*  | 983 *μs*       | 116.79 *ms* | Time taken to perform 300.000 deletions.  |

</div>

The chart below ilustrates clearly the behaviour designed, we can see how cost remains constant no matter the number of elements are inserted or deleted. Plus, spikes reveal when resize operations where made due to their high cost.

To make the chart clearer, the peaks have been cut off.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/stack/push_time.png" alt="push time"/>
</p>
<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/stack/pop_time.png" alt="pop time"/>
</p>

The space allocated in memory is shown in the charts below; It can be seen how memory grows and shrinks by chunks.

<div class="row">
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/stack/push_space.png" alt="push space"/>
  </div>
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/stack/pop_space.png" alt="pop space"/>
  </div>
</div>
