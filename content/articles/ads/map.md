---
title: Abstract Data Structures; The Map
date: 2023-04-06T00:00:00+00:00
draft: false
github_link: https://gitlab.com/ilcortegano/ads
author: Isauro López-Cortegano
description: ""
toc: 
---

# Map

**Maps** are a type of container designed to store data using **Key-Value** pairs. :mortar_board:

It can be imagined as a dictionary. Where a collection of words (the keys) are associated to a definition (the values).

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/map/map.png" alt="maps image"/>
</p>

## Design

The map interface defines five basic operations:

- Insert data, from now on: *insert(k: Key, v: Value) -> void*.
- Erase a key-value pair: *erase(k: Key) -> void*.
- Know if a key exists: *contains(k: Key) -> boolean*.
- Read a value, given the key: *at(k: Key) -> Value*.
- Count the number of elements: *size() -> number*.

### Red-Black Tree Container

For the map design a `Binary Tree`, more specifically a `Red-Black Tree`, can be used as container.

`Binary Trees` are tree data structures in which each parent `node` can have at most two children.
`Nodes` are structures of information. For this design, nodes defines the following variables:

- `key`: Key data from the key-value pair.
- `value`: Value data from the key-value pair.
- `left`: A link to the left `node`.
- `right`: A link to the right `node`.

A binary tree must respect the invariant:

```
tree(n) = empty(n) ∨ (key(n.left) < key(n) < key(n.right) ∧ tree(n.left) ∧ tree(n.right))
```

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/tree.png" alt="tree container"/>
</p>

Unlike linear data structures (Array, Linked List, Queues, Stacks, etc) which have only one logical way to traverse them, trees can be traversed in different ways. The following are the generally used methods for traversing trees:

<div align="center">

|          |                           |
| -------- | ------------------------- |
| InOrder  | 6, 11, 15, 23, 24, 38, 54 |
| PreOrder | 23, 11, 6, 15, 38. 24, 54 |
| PosOrder | 6, 15, 11, 24, 54, 38, 23 |

</div>

For each new element to be stored in the `binary tree`, a new `node` will be created.
To manage the nodes in an efficient way, we will make use of the following variables:

- `root`: A link to the last `root` node of the `tree`.
- `elements`: indicates how many elements are present in the `tree`.

The image below illustrates the behavior of a `binary tree` as it grows.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/tree_grow.png" alt="tree growth"/>
</p>

This data structure is efficient because it offers a logarithmic cost when locating, inserting and deleting elements.
If no further specification is provided, and we make use of a pure binary tree, the following may occur for successive ordered data insertions.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/tree_bad.png" alt="tree unbalanced"/>
</p>

In this case, the binary tree, although respecting the invariant, loses its effectiveness as a logarithmic structure, since it is used as a linked list of nodes and the cost of operations becomes linear.

To control these situations, `AVL trees` and `Red-Black trees` incorporate node balancing mechanisms, which correct malformations in the binary trees as elements are inserted and deleted.
This guarantees a logarithmic cost for the operations.
Some of the situations in which a binary tree gets unbalanced are illustrated below. For this, `Nodes` are tagged with a red or black `color` property to help identify these situations.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/common/tree_balance.png" alt="tree balanced"/>
</p>

### Hash Table Container

Another option is to use a `Hash Table`. `Hash Tables` are implemented a collection of lists which store key-value `pairs`.

A `Pair` is a structure of data that stores key-values as follows:
- `key`: holds the key data.
- `value`: holds the value data.

The `Pair` objects are stored in `Pools`, which are, `arrays` of pair objects. `Hash Tables` uses an `array` that can store `Pools`.
The idea of the hash table is to store the key-value pairs into pools.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/map/hash_table.png" alt="hash table image"/>
</p>

In order to know which pool the key-value pair needs to be inserted, a hash operation for the key is performed.
The operation modulus, `hash(key) % capacity`, tells which pool must be used.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/map/hash_table_insert.png" alt="hash table insert image"/>
</p>

The container's resize policy is subjet to design and context. Since there is no guarantee that all the pools are going to be used, it is posible to insert key-value pairs that always uses the same pool. In that case, the container behaves just like a list of key-value pairs and cost of operations becomes linear. To avoid this, the resize policy of a `Hash Table` triggers when the number of elements that are contained exceeds a threshold.

When the container resizes, the capacity increases and new pools are allocated. In this process, for each key-value pair in the container, the index of the pool it belongs to is re-evaluated. As a result, existing key-values spread across the pools reducing their length and descreasing the cost of future search operations. 

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/map/hash_table_resize.png" alt="hash table resize image"/>
</p>

### Design Tradeoffs

Most relevant tradeoffs are listed below:

<style>
	.prosTable tr {
		background: green;
	}
	.consTable tr {
		background: red;
	}
</style>

<div class="prosTable">

| Red-Black Tree | Hash Table |
| ---- | ------ |
| Cost of operations is O(log(n)).   | Average cost of operations is Θ(1).    |
| Efficient regarding memory space.  | Efficient regarding memory allocation. |

</div>

<div class="consTable">

| Red-Black Tree | Hash Table |
| ---- | ------ |
| Hard to implement.                          | Cost of operations is O(n).            |
| Operations allocate and de-allocate memory. | Memory does not shrink, even if empty. |
| Elements are scattered in memory.           | Uses many chunks of memory.            |

</div>

##### Other Map Designs

The standard design and specification of the [map](https://en.cppreference.com/w/cpp/container/map) is very well documented at [cppreference](https://en.cppreference.com).

Do not doubt on checking up the docs, they're amazing!

## Implementation

The following speudo-code outlines the realization of the `map` interface using a `red-black tree`.

```js
// Auxiliar function.
const insertElement = function(n: Node, k: Key, v: Value) {
	if (n == null) {             // If tree 'n' is empty..
		elements = elements + 1; // Count the new element.
		return new Node(k, v);   // Allocate the new node.
	} 

	if (isRed(n->left) && isRed(n->right)) // Break 4-nodes
		colorFlip(n);

	if (n->key == k) {       // If the element already exists..
		n->value = v;        // update the value.
	} else if (k < n->key) { // otherwise, 'k' is inserted in a sub-tree.
		n->left = insertElement(n->left, k, v);
	} else {
		n->right = insertElement(n->right, k, v);
	}

	// Rotates the tree if corrections are needed.
	if (isRed(n->right))
		n = rotateLeft(n);

	// Rotates the tree if corrections are needed.
	if (isRed(node->left) && isRed(n->left->left))
		n = rotateRight(n);

	return n; // Return the root of the current sub-tree.
}

// Auxiliar function.
const deleteElement = function(n: Node, k: Key) {

	if (k < n->key) { // if the element to erase is on the left children..
		// Rotates the tree if corrections are needed.
		if (!isRed(n->left) && !isRed(n->left->left))
			n = moveRedLeft(n);

		// Delete the element in the left sub-tree.
		n->left = deleteElement(n->left, k);
	} else {
		// Rotates the tree if corrections are needed.
		if (isRed(n->_left))
			n = rotateRight(n);

		if (k == n->key && n->right == null) { // if 'k' found and has no right childs..
			elements = elements - 1; // Discount the element.
			delete n;                // De-allocate the memory held by the node.
			return null;
		}

		// Rotates the tree if corrections are needed.
		if (!isRed(n->right) && !isRed(n->right->left))
			n = moveRedRight(n);

		if (k == n->key) {                 // if 'k' is found..
			let min: Node = min(n->right); // overwrite node 'k' with the minimum of sub-tree..
			n->key = min->key;
			n->value = min->value;
			n->right = deleteMin(n->right); // then, erase the minimum of the subtree.
		} else {
			n->right = deleteElement(n->right, k); // otherwise, look over the right node.
		}
	}
	return fixUp(n); // fix red-back invariant

}

function insert(k: Key, v: Value) {

	root = insertElement(root, k, v); // Insert element k-v onto the tree.
}

function erase(k: Key) {

	root = deleteElement(root, k); // Delete element k from the tree.
}

function contains(k: Key) {
	let curr: Node = root;                             // From the root node..

	while (curr != null && k != curr->key) {           // navigate to the left or right children..
		curr = k < curr->key? curr->left: curr->right; // looking for the element 'e'.
	}
	return curr != null;                               // return true if found, false otherwise.
}

function at(k: Key) {
	let curr: Node = root;                             // From the root node..

	while (curr != null && k != curr->key) {           // navigate to the left or right children..
		curr = k < curr->key? curr->left: curr->right; // looking for the key 'k'.
	}
	if (curr == null)
		throw error;
	return curr.value;                                 // return the value if found, error otherwise.
}

function size() {
	return elements; // Return the number of elements.
}
```

The following speudo-code outlines the realization of the `map` interface using a `hash-table`.

```js
function insert(k: Key, v: Value) {
	let index: int = hask(k);      // get the hash index for the key 'k'

	if (_pools[index] == null)     // if the pool does not exists..
		pools[index] = new Pool(); // create a pool.

	// Insert or update the key-value pair.
	let inserted: bool = pools[index]->insertOrUpdate(k, v);
	if (inserted)				 // If a new key-value item was inserted
		elements = elements + 1; // count the new element.
}

function erase(k: Key) {
	let index: int = hask(k);     // get the hash index for the key 'k'

	if (_pools[index] != null) {  // if the pool exists..
		elements = elements - 1;  // reduce the count of elements
		pools[index]->erase(key); // delete the key-pair from the pool.
	}
}

function contains(k: Key) {
	let index: int = hask(k); // get the hash index for the key 'k'

	if (pools[index] == null) // if the key is not found, return false.
		return false;

	let pIndex: int = pools[index]->search(k); // search the key on the pool.
	return pools[index]->contains(k);          // return true if the 'k' is present in the pool.
}

function at(k: Key) {
	let index: int = hask(k); // get the hash index for the key 'k'

	if (pools[index] == null) // if the key is not found..
		throw error;          // throw an error.

	let pIndex: int = pools[index]->search(k); // search the key on the pool.
	return pools[index][pIndex]->value;        // return the key's value.
}

function size() {
	return elements; // Return the number of elements.
}
```

## Performance

### Operation Costs

<div align="center">

   | Name         | *O*(tree)    | *O*(hash-table)    |  Purpose                                      |
   | ------------ | ------------ | ------------------ | --------------------------------------------- |
   | *insert*     | *O*(log(n))  | *O*(n) & *Θ*(1)    | Insert/Update a key-value pair.               |
   | *erase*      | *O*(log(n))  | *O*(n) & *Θ*(1)    | Delete a key-value pair.                      |
   | *contains*   | *O*(log(n))  | *O*(n) & *Θ*(1)    | Check if a given key exists in the collection.|
   | *at*         | *O*(log(n))  | *O*(n) & *Θ*(1)    | Reads the value hold by the a given key.      |
   | *size*       | *O*(1)       | *O*(1)             | Gets the number of elements in the container. |

</div>

### Benchmarks

To test the proposed design, the total time consumed by push and pop operations was timed by performing 300.000 insertions and deletions. These results are contrasted with those obtained using the standard implementation.
Note that the repository code is not exception-safe and should not be used in production environments.

The overall performance is summarized in this table.

<div align="center">

   |          | ilc::ads:tree_map | ilc::ads:hash_map | std::map    | Description                               |
   | -------- | ----------------- | ----------------- | ----------- | ----------------------------------------- |
   | *insert* | 180 *ms*          | 1104 *ms*         | 159 *ms*    | Time taken to perform 300.000 insertions. |
   | *erase*  | 20.79 *s*         | 455 *ms*          | 20.72 *s*   | Time taken to perform 300.000 deletions.  |

</div>

### RB-Tree: Benchmarks

The chart below ilustrates the time taken by the operations when using the `red-black tree` container.
It can be seen how the cost grows logarithmically as the number of elements grows within the container.
In the case of the deletion graph, deleting nodes scattered in memory is time consuming, causing the logarithmic cost of the operation to be unappreciated. The deletion chart should look like a mirror image of the insertion one.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/map/tree_insert_time.png" alt="tree insert time"/>
</p>
<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/map/tree_erase_time.png" alt="tree erase time"/>
</p>

The space allocated in memory is shown in the charts below; It can be seen how memory grows at a constant rate as new nodes are being created or destroyed.

<div class="row">
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/map/tree_insert_space.png" alt="tree insert space"/>
  </div>
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/map/tree_erase_space.png" alt="tree erase space"/>
  </div>
</div>

### Hash-Table: Benchmarks

The chart below ilustrates the time taken by the operations when using the `hash-table` container.
The chart below ilustrates the time taken by the operations, the time cost remains constant no matter the number of elements are inserted or deleted. Plus, spikes reveal when resize operations of the hast table where made due to their high cost.

To make the chart clearer, the peaks have been cut off.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/map/hash_insert_time.png" alt="hash insert time"/>
</p>
<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/map/hash_erase_time.png" alt="hash erase time"/>
</p>

The space allocated in memory is shown in the charts below; It can be seen how memory grows and shrinks by chunks as new pools are being created. However, deletion operations does not free the existing pools, that's why, memory space remains constant.

<div class="row">
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/map/hash_insert_space.png" alt="hash insert space"/>
  </div>
  <div class="col-sm-6">
    <img class="img-fluid" src="/images/blogs/ads/map/hash_erase_space.png" alt="hash erase space"/>
  </div>
</div>