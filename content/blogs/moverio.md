---
title: Gesture Identification for Interaction with Augmented Reality Systems through Hand Tracking
date: 2022-08-30T00:00:00+00:00
draft: true
github_link: https://gitlab.com/ilcortegano/tfm
author: Isauro López-Cortegano
tags:
  - Python
  - Android
  - AR
image: /images/blogs/bt35e/moverio-arch.png
description: ""
toc: 
---

Coming soon