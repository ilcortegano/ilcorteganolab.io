---
title: The relevance of Abstract Data Structures
date: 2023-01-16T00:00:00+00:00
draft: false
github_link: https://gitlab.com/ilcortegano/ads
author: Isauro López-Cortegano
image: /images/blogs/ads/ads.png
description: ""
toc: 
---

## Introduction

In this project I talk about relevance, performance and tradeoffs concerning the nature of different abstract data structures. From the conceptual design to its final implementation.

In order to make this blog accesible, I will try to keep things simple. From now on, it is mandatory for the reader to understand the concepts of "computational complexity" and "abstract data structures".

### Complexity

In computer science, the complexity measures the amount of resources required for a particular algorithm to run.
Depending on the cost of the resources involved, algorithms are classified following the chart ilustrated below.

<p align="center">
  <img class="img-fluid" src="/images/blogs/ads/complexity.jpg" alt="computational complexity"/>
</p>

As coloured in the chart, costs are rated by colours. From red (horrible), to yellow (fair), to green (excellent). Ideally, the algorithms should fall into the range [ *O*(1), *O*(n) ].

We will use from now on this notation to measure the complexity of algorithms.

<div align="center">

   | Symbol  | Name        | Meaning                                               |
   | ------- | ----------- | ----------------------------------------------------- |
   | *Ω*     | Big Omega   | *Ω*(N) means it takes at least N steps.               |
   | *Θ*     | Big Theta   | *Θ*(N) means N is the amortized cost.                 |
   | *O*     | Big Oh      | *O*(N) means it takes at most N steps.                |

</div>

### Abstract Data Structures

Abstract data structures are, as the name suggests, data containers. Their purpose is simple: encapsulate, manage and provide data in an efficient way.

Data structures are designed by defining an operations interface which satisfies needs or requirements, then studying which structures must be used and how to manage them in order to realise an efficient implementation.
The following table contains links to articles, these go into more detail about the container.

<div align="center">

   | Name | Description | |
   | ----------------------- | ----------- | ------- |
   | [**Stacks**](/articles/ads/stack) | [**Stacks** operates in a **LIFO** (*Last in, first out*) context. :mortar_board:](/articles/ads/stack) | [![stacks image](/images/blogs/ads/stack/stacks.png)](/articles/ads/stack) |
   | [**Queues**](/articles/ads/queue) | [**Queues** operates in a **FIFO** (*First in, first out*) context. :mortar_board:](/articles/ads/queue) | [![queues image](/images/blogs/ads/queue/queue.png)](/articles/ads/queue) |
   | [**Heaps**](/articles/ads/heap) | [**Heaps** operates in a **PRIO** (*In order of priority*) context. :mortar_board:](/articles/ads/heap) | [![heaps image](/images/blogs/ads/heap/heap.png)](/articles/ads/heap) |
   | [**Lists**](/articles/ads/list) | [**Lists** stores data in an unordered manner. :mortar_board:](/articles/ads/list) | [![lists image](/images/blogs/ads/list/list.png)](/articles/ads/list) |
   | [**Sets**](/articles/ads/set) | [**Sets** are collections of **unique** items. :mortar_board:](/articles/ads/set) | [![sets image](/images/blogs/ads/set/set.png)](/articles/ads/set) |
   | [**Maps**](/articles/ads/map) | [**Maps** are collections of **Key-Value** pairs. :mortar_board:](/articles/ads/map) | [![maps image](/images/blogs/ads/map/map.png)](/articles/ads/map) |

</div>

Each data structure has a context in which it excels.

Variants of the above, as well as new ones, can be implemented. At the end of the day, what matters is to understand the nature, the utility and the context of each data structure in order to provide good solutions to problems.

Good solutions are good solutions because they satisfy requirements in an efficient way.

Doing a job in an efficient way is quite often a hard thing to do.
We are often compelled to make tradeoffs, either in terms of memory space, runtime, readability of code, scalability and maintainability of a program/system.

Knowing and studying different data structures helps to provide better solutions to problems.

## Bibliography

- *M. J. Magazine, G. L. Nemhaus, and L. E. Trotte. When the Greedy Solution Solves a Class of Knapsack Problems. Operations Research, 1975*.
- *G. Brassard, and P. Bratley. Fundamentos de algoritmia. Prentice Hall, 1997*.
- *E. Horowitz, S. Shani, and D. Mehta. Fundamentals of Data Structures in C++. Computer Science Press, 4th edition, 1997*.
- *E. Horowitz, S. Shani, and S. Rajasekaran. Computer Algorithms. Computer Science Press, 1998*.
- *M. A. Weiss. Estructuras de datos en Java. Addison Wesley, 2000*.
- *T. H. Cormen, C. E. Leiserson, R. L. Rivest, and C. Stein. Introduction to Algorithms. MIT Press, 2nd edition, 2001*.
- *R. Neapolitan, and K. Naimipour. Foundations of Algorithms using C++ pseudocode. Jones and Bartlett Publishers, 3rd edition, 2004*.
- *R. Peña. Diseño de Programas: Formalismo y Abstracción. Pearson Prentice Hall, 3rd edition, 2005*.
- *R. Sedgewick, and K. Wayne. Algorithms. 4th edition, 2011*.
- *N. Martí Oliet, Y. Ortega Mallén, and J. A. Verdejo López. Estructuras y datos y métodos algorítmicos: 213 ejercicios resueltos. Garceta Grupo Editorial, 2nd edition, 2013*.
